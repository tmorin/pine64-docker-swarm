# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/tmorin/pine64-docker-swarm/compare/v1.1.0...v1.2.0) (2020-02-14)


### Features

* add memory reservation ([bdf3f46](https://gitlab.com/tmorin/pine64-docker-swarm/commit/bdf3f46a42ea20e91a806574d5549fadd645e065))
* decrease log level for the monitoring stack ([d605368](https://gitlab.com/tmorin/pine64-docker-swarm/commit/d605368b24673ef5315c00eb0f494f03d716c03a))
* switch to docker image thibaultmorin/keepalived ([e46b791](https://gitlab.com/tmorin/pine64-docker-swarm/commit/e46b791d3e3ce559c4a7470482dcb24fb1eb5348))
* upgrade keepalived version and docker image ([06a7ba7](https://gitlab.com/tmorin/pine64-docker-swarm/commit/06a7ba790c6639c4b48e20af9c21b471f25d9186))
* upgrade the docker version ([9aab7fd](https://gitlab.com/tmorin/pine64-docker-swarm/commit/9aab7fd1d32c1b50c2e9642a17d6293dcfaf5e10))
* upgrade to traefik v2 ([e5dd029](https://gitlab.com/tmorin/pine64-docker-swarm/commit/e5dd029d4a33e188b3f4ee9ab8f9e974f56164b4))
* **ceph:** ceph files are persisted on disk to support hard reboot ([5075c20](https://gitlab.com/tmorin/pine64-docker-swarm/commit/5075c206b0c91342b4f05ce017017bd563a07cbe))
* **common:** deactivate swap on boot ([1a2d599](https://gitlab.com/tmorin/pine64-docker-swarm/commit/1a2d599bee686b16c1244abf5338bd2381c6a60c))
* **common:** upgrade docker version ([d4bd54a](https://gitlab.com/tmorin/pine64-docker-swarm/commit/d4bd54a102e45ee29164ea3421c47e57d9819876))


### Bug Fixes

* chronograph was not exposed ([ac4b836](https://gitlab.com/tmorin/pine64-docker-swarm/commit/ac4b8368f8f06148b56fab28d520e20ae7f47c72))
* fix bad typo ([542589d](https://gitlab.com/tmorin/pine64-docker-swarm/commit/542589d299c8431b1968118377a7c76778402891))
* keepalived could not use lvs ([28998c0](https://gitlab.com/tmorin/pine64-docker-swarm/commit/28998c05cb1622a8ca431d7dd6003b8dac7d865d))
* **ceph:** fix variable name about ceph image ([a6e940b](https://gitlab.com/tmorin/pine64-docker-swarm/commit/a6e940b3bd45eb0710443c858306ed17c13168f1))
* **ceph:** increase the memory of osd containers to 500M ([ca84acc](https://gitlab.com/tmorin/pine64-docker-swarm/commit/ca84acc6b6edb0e335b9c8488e348a8180e1f5a5))
* **ceph:** increase the wait for value when stack is loading ([4e7a7f9](https://gitlab.com/tmorin/pine64-docker-swarm/commit/4e7a7f92e7efd3e47742ed2864348ed4779fcab5))
* add more metrics ([b7b4031](https://gitlab.com/tmorin/pine64-docker-swarm/commit/b7b40310e83ff0966d96f07767da3a8a56fc5ed8))
* increase again ram for ceph osd ([96f3b80](https://gitlab.com/tmorin/pine64-docker-swarm/commit/96f3b80fa33c26e3b0415ae48578be690bd644e9))
* increase ram for ceph osd ([2ccf368](https://gitlab.com/tmorin/pine64-docker-swarm/commit/2ccf368390c8f65603ed685484d92df661f0807b))

# [1.1.0](https://gitlab.com/tmorin/pine64-docker-swarm/compare/v1.0.0...v1.1.0) (2019-03-27)


### Bug Fixes

* "resources" is not valid in portainer stack file ([3ed7eba](https://gitlab.com/tmorin/pine64-docker-swarm/commit/3ed7eba))
* add timer beore to mount cephfs ([265f204](https://gitlab.com/tmorin/pine64-docker-swarm/commit/265f204))
* add variable for portainer's host ([e6611fd](https://gitlab.com/tmorin/pine64-docker-swarm/commit/e6611fd))
* ceph installation ([77cd070](https://gitlab.com/tmorin/pine64-docker-swarm/commit/77cd070))
* ceph is not place at in the right order ([70a0b75](https://gitlab.com/tmorin/pine64-docker-swarm/commit/70a0b75))
* ceph order ([8d8d8ca](https://gitlab.com/tmorin/pine64-docker-swarm/commit/8d8d8ca))
* fix keepalived image ([eff4d77](https://gitlab.com/tmorin/pine64-docker-swarm/commit/eff4d77))
* increase the log level of traefik ([cec63f0](https://gitlab.com/tmorin/pine64-docker-swarm/commit/cec63f0))
* limit memory resource for ceph and monitoring ([e81a283](https://gitlab.com/tmorin/pine64-docker-swarm/commit/e81a283))
* limit memory resource for monitoring ([05f19bf](https://gitlab.com/tmorin/pine64-docker-swarm/commit/05f19bf))
* remove use less config entry ([860bfe7](https://gitlab.com/tmorin/pine64-docker-swarm/commit/860bfe7))
* the traefik ports are attached to the host not to the swarm ([b2f5633](https://gitlab.com/tmorin/pine64-docker-swarm/commit/b2f5633))


### Features

* add monitoring stack with alert ([d8d81d6](https://gitlab.com/tmorin/pine64-docker-swarm/commit/d8d81d6))
* add mount of cifs filesystem (samba share for instance) ([5587272](https://gitlab.com/tmorin/pine64-docker-swarm/commit/5587272))
* deactivation of the swap ([2b4a836](https://gitlab.com/tmorin/pine64-docker-swarm/commit/2b4a836))
* split to other playbooks ([59c9eb8](https://gitlab.com/tmorin/pine64-docker-swarm/commit/59c9eb8))
* upgrade ceph daemon ([05b73d3](https://gitlab.com/tmorin/pine64-docker-swarm/commit/05b73d3))



<a name="1.0.0"></a>
# 1.0.0 (2018-10-29)


### Features

* bootstrap a ready to use Docker Swarm cluster ([aa43f07](https://gitlab.com/tmorin/pine64-docker-swarm/commit/aa43f07))
