#!/usr/bin/env bash

CEPHFS_DATA_PG_NUM=${1:-200}
CEPHFS_DATA_SIZE=${2:-2}
CEPHFS_METADATA_PG_NUM=${3:-64}
CEPHFS_METADATA_SIZE=${4:-2}

OUTPUT_DIR=/tmp/ceph_stack

mon_id=`docker ps -qf name=ceph_mon`
mon_shell="docker exec -it ${mon_id} "

${mon_shell} ceph -s

if [ $? -ne 0 ]; then
    exit 1
fi

${mon_shell} ceph osd lspools | grep "cephfs_data"
if [ $? -ne 0 ]; then
  echo "--- create pool cephfs_data"
  ${mon_shell} ceph osd pool create cephfs_data ${CEPHFS_DATA_PG_NUM}
  ${mon_shell} ceph osd pool set cephfs_data size  ${CEPHFS_DATA_SIZE}
  ${mon_shell} ceph osd pool set cephfs_data nodeep-scrub 1
fi

${mon_shell} ceph osd lspools | grep "cephfs_metadata"
if [ $? -ne 0 ]; then
  echo "--- create pool cephfs_metadata"
  ${mon_shell} ceph osd pool create cephfs_metadata ${CEPHFS_METADATA_PG_NUM}
  ${mon_shell} ceph osd pool set cephfs_metadata size  ${CEPHFS_METADATA_SIZE}
fi

${mon_shell} ceph fs ls | grep "cephfs_data"
if [ $? -ne 0 ]; then
  echo "--- create fs"
  ${mon_shell} ceph fs new cephfs cephfs_metadata cephfs_data
fi

${mon_shell} ceph fs authorize cephfs client.swarm / rw > ${OUTPUT_DIR}/ceph.client.swarm.keyring
sed -rn "s/key = (.+)/\1/p;" ${OUTPUT_DIR}/ceph.client.swarm.keyring | xargs > ${OUTPUT_DIR}/ceph.client.swarm.key
