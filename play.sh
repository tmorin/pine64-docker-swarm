#!/usr/bin/env bash

if [ -f "custom_variables.yml" ]; then
    BOOK=${1}
    shift
    echo ansible-playbook ${BOOK} --extra-vars "@custom_variables.yml" $@
    ansible-playbook ${BOOK} --extra-vars "@custom_variables.yml" $@
else
    echo ansible-playbook $@
    ansible-playbook $@
fi


